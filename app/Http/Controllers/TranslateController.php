<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslateController extends Controller
{
    public function translate($string)
    {
        $into = (Input::get('into'));
        $from = Input::get('from');
        $translate = new GoogleTranslate();
        $translate->setSource($from);
        $translate->setTarget($into);
        $response['status']='success';
        $response['translate_result'] = $translate->translate($string);
        return $response;
    }
}
